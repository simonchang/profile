<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <title></title>
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="menu.js"></script>
</head>
<body>

<style type="text/css">
* { margin:0;
    padding:0;
}
html { background:#b6b7bc; }
body {
    margin:0px auto;
    width:814px;
    height:1500px;
    overflow:hidden;
    background:#fff url(images/back.jpg) no-repeat;
}
div#menu {
    margin:64px 0 0 0px;
    position:absolute;
}
div#jquery {
    font:11px 'Trebuchet MS';
    color:#fff;
    text-align:center;
    clear:left;
    position:absolute;
    top:546px;
    width:560px;
}
div#jquery a { color:#ffffff; }
div#jquery a:hover { color:#fff; }

ul, ol, dl { 
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 
	padding-right: 15px;
	padding-left: 15px; 
}
a img { 
	border: none;
}

a:link {
	color: #42413C;
	text-decoration: underline; 
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus { 
	text-decoration: none;
}

.container {
	width: 800px;
	background-color: #000000;
	margin: 0 auto; 
}

header {
	background-color: #363636;
}

.sidebar1 {
	float: left;
	width: 180px;
	background-color: #EADCAE;
	padding-bottom: 10px;
}
.content {
	padding: 10px 0;
	width: 600px;
	float: left;
}
aside {
	float: left;
	width: 180px;
	background-color: #EADCAE;
	padding: 10px 0;
}


.content ul, .content ol {
	padding: 0 15px 15px 40px; 
}


ul.nav {
	list-style: none; 
	border-top: 1px solid #666; 
	margin-bottom: 15px; 
}
ul.nav li {
	border-bottom: 1px solid #666; 
}
ul.nav a, ul.nav a:visited { 
	padding: 5px 5px 5px 15px;
	display: block; 
	width: 160px;  
	text-decoration: none;
	background-color: #C6D580;
}
ul.nav a:hover, ul.nav a:active, ul.nav a:focus { 
	background-color: #ADB96E;
	color: #FFF;
}

/* ~~ 頁尾 ~~ */
footer {
	padding: 10px 0;
	background-color: #CCC49F;
	position: relative;
	clear: both;
}
/* ~~ 其他 float/clear 類別 ~~ */
.fltrt {  
	float: right;
	margin-left: 8px;
}
.fltlft { 
	float: left;
	margin-right: 8px;
}
.clearfloat { 
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}

/*HTML 5 支援 – 設定新的 HTML 5 標籤以顯示區塊並使瀏覽器能夠正確顯示標籤。 */
header, section, footer, aside, article, figure {
	display: block;
}
-->
</style>
<div id="header">    
<div id="menu">   
	<ul class="menu">        
        <li><a href="http://macrotech.herokuapp.com/"><span>首頁</span></a></li>
        <li><a href="content.php" target="_self"><span>重大歷程</span></a></li>
		<li><a href="content-is.php" target="_self"><span>資訊工程</span></a></li>
		<li><a href="content-green.php" target="_self"><span>綠能機電</span></a></li>
		<li><a href="content-eng.php" target="_self"><span>機電工程</span></a></li>
		<li><a href="content-sales.php" target="_self"><span>業務</span></a></li>
		<li><a href="#"><span>證照</span></a></li>
        <li><a href="http://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=4070422648363f683c583a1d1d1d1d5f2443a363189j48&jobsource=checkc" target="_new"><span>人力資源</span></a></li>
		<li><a href="contact-us.php" target="_self"><span>連絡我們</span></a></li>
        <li><a href="#"><span>簡體</span></a></li>
        <li><a href="#"><span>English</span></a></li>    	
    </ul>
</div><br><br><br><br><br><br><br><div class="main" style="overflow: auto; border: solid 0px red; scrollbar-face-color: white; font-size: 12px; text-align: left; font-family: Arial, Helvetica, sans-serif;">
            
    <div id="MainContent_UpdatePanel1">
	
            <div id="MainContent_div_List">
                <fieldset>
                    <legend>                    <strong><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />資訊工程系統部</strong>:<br>
                    <br>
                  </legend>
                    <table width="98%" border="1" align="center" cellpadding="1" cellspacing="1">
                      <tbody>
                        <tr>
                          <td valign="top" width="172"><p><span lang="EN-US"><u></u></span><strong><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" /></strong>資訊工程系統部</p></td>
                          <td valign="top" width="152"><p>營業能力服務項目 <span lang="EN-US"><u></u><u></u></span></p></td>
                          <td valign="top" width="91"><p>業務範圍優點強項說明<span lang="EN-US"><u></u><u></u></span></p></td>
                          <td valign="top" width="83"><p>實績<span lang="EN-US"> / </span>相片檔聯結<span lang="EN-US"><u></u><u></u></span></p></td>
                          <td valign="top" width="116"><p><span lang="EN-US"><span style="text-align: center"> </span></span><span style="text-align: center">English Reference</span></p></td>
                        </tr>
                        <tr>
                          <td valign="top" width="172">
<img src="http://www.so-power.com/Img/S8.jpg" width="172" height="156" /></td>
                          <td valign="top" width="152"><p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />智慧型雲端電信機房規劃設計承造 <span lang="EN-US"><u></u><u></u></span></p>
                            <p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />廠房大樓 弱電工程規劃設計承造<span lang="EN-US"><u></u><u></u></span></p>
                            <p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />機房設計<span lang="EN-US">: </span>電信、網路<a name="139d2ec27a90e7c9_OLE_LINK1"></a><a name="139d2ec27a90e7c9_OLE_LINK2"></a>、門禁 、監視<span lang="EN-US"><u></u><u></u></span></p>
                            <p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />環控系統等建置<span lang="EN-US"><u></u><u></u></span></p>
                            <p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />不斷電系統工程與設備<span lang="EN-US"><u></u><u></u></span></p></td>
                          <td valign="top" width="91"><p><span lang="EN-US">-</span>多年機房建置經驗<span lang="EN-US"><u></u><u></u></span></p>
                            <p>可依客戶需求 客製高信賴高可靠度與穩定系統 <span lang="EN-US"><u></u><u></u></span></p>
                            <p><span lang="EN-US">-</span>整體設計規劃具未來擴充性<span lang="EN-US">.<u></u><u></u></span></p>
                            <p><span lang="EN-US">-<u></u><u></u></span></p>
                            <p><span lang="EN-US"><u></u> <u></u></span></p></td>
                          <td valign="top" width="83"><p><a href="http://macrotech.herokuapp.com/1.JPG" target="_new">實績照片1</a><br>
                            <a href="http://macrotech.herokuapp.com/00002.JPG" target="_new">實績照片2</a><br>
                            <a href="http://macrotech.herokuapp.com/00003.JPG" target="_new">實績照片3</a><br>
                            <a href="http://macrotech.herokuapp.com/00004.JPG" target="_new">實績照片4</a><br>
                            <a href="http://macrotech.herokuapp.com/00005.JPG" target="_new">實績照片5</a><br>
                            <a href="http://macrotech.herokuapp.com/00006.JPG" target="_new">實績照片6</a><br>
                            <a href="http://macrotech.herokuapp.com/00007.jpg" target="_new">實績照片7</a><br>
                            <a href="http://macrotech.herokuapp.com/00008.JPG" target="_new">實績照片8</a><br>
                            <a href="http://macrotech.herokuapp.com/00009.JPG" target="_new">實績照片9</a><br>
                            <a href="http://macrotech.herokuapp.com/00010.JPG" target="_new">實績照片10</a><br>
                            <a href="http://macrotech.herokuapp.com/00011.JPG" target="_new">實績照片11</a><br>
                            <a href="http://macrotech.herokuapp.com/00012.JPG" target="_new">實績照片12</a><br> <a href="http://macrotech.herokuapp.com/00013.JPG" target="_new">實績照片13</a><br> <a href="http://macrotech.herokuapp.com/00014.JPG" target="_new">實績照片14</a>
                            <span><br>
                          </span></a></p></td>
                          <td valign="top" width="116"><p><span lang="EN-US">Smart telecom machine room design and construction<u></u><u></u></span></p>
                            <p><span lang="EN-US">Machine room telecom</span><span lang="EN-US"> </span>、<span lang="EN-US">networkin</span><span lang="EN-US">g</span>、<span lang="EN-US">     security</span><span lang="EN-US"> </span>、<span lang="EN-US">environment    monitoring system .<u></u><u></u></span></p>
                            <p><span lang="EN-US">UPS system sale and technical support </span></p></td>
                        </tr>
                      </tbody>
                  </table>
                  <legend><br>
                  </legend>
                </fieldset>
            </div>
        
</div>

        </div>
</div>
<div id="jquery"><a href="http://apycom.com/">.</a></div>
</body>
</html>